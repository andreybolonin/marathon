<?php
if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])){
    $pdo = new PDO("pgsql:host=127.0.0.1;dbname=crud", 'marathon', 'marathon2020');
    $stmt = $pdo->prepare('UPDATE article SET `name` = :name, `description` = :description, `created_at` = :created_at WHERE `id` = :id');
    $stmt->bindValue(':name', $_POST['name']);
    $stmt->bindValue(':description', $_POST['description']);
    $stmt->bindValue(':created_at', $_POST['created_at']);
    $stmt->bindValue(':id', $_POST['id']);
    $stmt->execute();
    header('Location: index.php');
}
if (isset($_GET['id'])){
    $pdo = new PDO("pgsql:host=127.0.0.1;dbname=crud", 'marathon', 'marathon2020');
    $stmt = $pdo->prepare('SELECT * FROM article WHERE `id`  = :id LIMIT 1');
    $stmt->bindValue(':id', (int) $_GET['id']);
    $stmt->execute();
    $article = $stmt->fetch();
}
else{
    header('Location: index.php');
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create</title>
</head>
<body>
<form method="post">
    <input type="hidden" required name="id" value="<?=$_GET['id']?>">
    <div class="form-row">
        <label>Назва</label>
        <input type="text" required name="name" placeholder="name" value="<?=$article['name']?>">
    </div>
    <div class="form-row">
        <label>Опис</label>
        <input type="text" required name="description" placeholder="description"  value="<?=$article['description']?>">
    </div>
    <div class="form-row">
        <label>Дата створення</label>
        <input type="text" required name="created_at" placeholder="Дата створення" value="<?=$article['created_at']?>">
    </div>
    <div class="form-row"><input type="submit"></div>
</form>
</body>
</html>